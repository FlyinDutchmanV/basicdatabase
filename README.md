# basicdatabase

README

included:

All source files (.json, .js)

this README

appropriate image files (.png)



Contact Tracing Logbook

Description

The Contact Tracing Logbook is an Alexa skill that generates a record as an Item in an Amazon DynamoDBTable. While primarily responsible for efficiently facilitating the logging of visitor information, the Contact Tracing Logbook skill truly excels at creating a time(date)-detailed entry from three spoken parameters.



Requirements

Alexa developer console account (see https://developer.amazon.com/docs/app-submission/manage-account-and-permissions.html)

AWS account (see https://docs.aws.amazon.com/a4b/latest/ag/console_signup.html)


*Also, required is the setting up of relevant IAM User Persmissions and Policies for communication between an AWS-hosted Lambda Function and assosciated Alexa developer skill. (see https://docs.aws.amazon.com/IAM/latest/UserGuide/access.html).

*The DynamoDBTable should be created under the AWS account holder's account, with name of DemoDatabase, and stipulated Item regions as shown in the attached DemoDatabase.png file.

*An IDE is strongly recommended for implementation of the AWS Lambda Function and corresponding dependencies.



Examples

The Contact Tracing Logbook skill is open via its invocation name of 'contact tracing logbook'. A sample of a test scenario was attached in the TestScenario.png file where the vocal commands to skill are shown using the Alexa developer console.

The skill started via the Alexa action of open and then invocation name 'contact tracing logbook'. An unrecognised command of 'what' was spoken and the Alexa skill reprompted with a more specific instruction. This was done twice, to verify that it returns to the LaunchRequesthandler 'module'. The LogbookIntent handler which deals with receiving and writing information to the DynamoDBTable, was triggered using the 'add ' then name utterance. The skill then walked through the process of getting the visitor's contact number information and visitor contact information status. Next, the HelpIntent of the skill was triggered using the keyword 'help'. It successfully returned the expected assistance information for the user of the Contact Tracing Logbook skill. The LogbookIntent handler was again triggered. This instance however using the 'add' name, contact number info., and then contact status utterance. Lacking the contact status, the visitor was prompted for the information. Finally, the skill was closed using the stop utterance and returned a termination message. The results of the Item logging were stored in the DemoDatabase Table as shown in the file TestScenario_UpdatePerformed.png file.

N.B. Not shown, but also tested were the DatebookIntent and NamebookIntent handlers which are responsible for retrieving the information to be searched for. They were triggered using the 

-'date of ', then day, month, year

-'name of ', then name

utterances, respectively.



Contributing

- Developement Team of the Top Engineers Committee 

- SigmaIDE hosted by SlappForge

The Top Engineers Committee's development team consisting of Baldwin Alcindor, Johann Estwick and Jason Moore were set about with designing and devloping an Alexa skill with the required capabilities to allow for contact tracing, with the stipulated requirements. With contribution from Alyssa Armstrong, Baldwin, Johann and Jason, a meeting meeting was held to collaboratively produce a design requirements document which would act as a framework for the implemented product. Jason with role of software engineer, set about developing methods to accurately record and store the data input from a user into an Amazon S3 bucket. Thus, a user interaction module that could store the DynamoDBTable's neccessary data was created.

Using the SigmaIDE to facilitate working with Amazon Lambda Function, data entry methods for the DynamoDBTable ("DemoDatabase") were created within which the assosciated Items are stored. Two other Intents were created which handled user requests for information on specification of either date or name. With no implementation into the DynamoDB database, these search functions currently test the capabilities to verify the information being searched for. The relevant README and documentation files we produced along with attached documented testing.