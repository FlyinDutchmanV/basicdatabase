const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
const Alexa = require('ask-sdk');
let skill;

exports.handler = async function (event, context) {
    if (!skill) {
        skill = Alexa.SkillBuilders.custom()
            .addErrorHandlers(ErrorHandler)
            .addRequestHandlers(
                LaunchRequestHandler,
                HelpIntentHandler,
                CancelAndStopIntentHandler,
                FallbackIntentHandler,
                LogbookIntentHandler,
                DatebookIntentHandler,
                NamebookIntentHandler
            )
            .withApiClient(new Alexa.DefaultApiClient())
            .create();
    }

    const response = await skill.invoke(event, context);
    return response;
};


// Speaks the individual digits of the long ID numbers for clarity
// Utilised through 'numberDigit = spellDigitOutput(number);'
function spellDigitOutput(number) {
    return '<say-as interpret-as="digits">' + number + '</say-as>';
}


// Runs when the LogbookIntent is called
// This corresponds to an input of name, contact id info, and visitor status
const LogbookIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'LogbookIntent';
    },
    async handle(handlerInput) {
        const serviceClientFactory = handlerInput.serviceClientFactory;
        const deviceId = handlerInput.requestEnvelope.context.System.device.deviceId;

        let userTimeZone;

        // Dependent on access to System Time Zone - unable to record data otherwise
        try {
            const upsServiceClient = serviceClientFactory.getUpsServiceClient();
            userTimeZone = await upsServiceClient.getSystemTimeZone(deviceId);
        } catch (error) {
            if (error.name !== 'ServiceError') {
                return handlerInput.responseBuilder.speak("There was a problem connecting to the service.").getResponse();
            }
            console.log('error', error.message);
        }

        console.log('userTimeZone', userTimeZone);

        // Getting the current date with the time
        const currentDateTime = new Date(new Date().toLocaleString("en-US", { timeZone: userTimeZone }));
        
        // Array of month names, used to convert numeric value from getMonth function
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        const day = currentDateTime.getDate();
        const month = monthNames[currentDateTime.getMonth()];
        const year = currentDateTime.getFullYear();
        const timeLog = currentDateTime.getTime();
        const name = Alexa.getSlotValue(handlerInput.requestEnvelope, 'name');
        const contact = Alexa.getSlotValue(handlerInput.requestEnvelope, 'contact');
        const status = Alexa.getSlotValue(handlerInput.requestEnvelope, 'status');

        // Resposible for creating a new Item in the DynamoDB with the data provided
        try {
            let data = await ddb.put({
                TableName: "DemoDatabase",
                Item: {
                    logTime: timeLog,
                    contact: contact,
                    day: day,
                    month: month,
                    name: name,
                    status: status,
                    year: year
                }
            }).promise();

        } catch (err) {
            // Currently, a value will always be passed, so no error handling needed
        };

        // Speaks the individual digits of the long ID numbers for clarity
        let contactDigits = 0;
        contactDigits = spellDigitOutput(contact);

        const speechText = 'Thank you, I will register the name ' + name + ' to the ' + day + 'th ' 
            + month + ', ' + year + '. Your ' + status + ' information of ' + contactDigits + ' has also been logged';
        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};


// Runs when the Amazon provided HelpIntent is called - give a brief decription, and direction when using skill
const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'This is the Contact Tracing skill from the Top Engineering Committee. You can search our database for created records '
            + ' by saying name of or date of. Also, you can enter your own record by saying add and following up with your name';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};


// Runs when the Amazon provided CancelIntent or StopIntent is called - confirm termination to user
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Goodbye!';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};


// Runs when the Amazon provided FallbackIntent is called
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Please provide your name, contact information and status, in that order';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};


// Runs when DatebookIntent is called - Search for records that satisfy the date conditions
const DatebookIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'DatebookIntent';
    },
    handle(handlerInput) {
        const searchDay = Alexa.getSlotValue(handlerInput.requestEnvelope, 'searchDay');
        const searchMonth = Alexa.getSlotValue(handlerInput.requestEnvelope, 'searchMonth');
        const searchYear = Alexa.getSlotValue(handlerInput.requestEnvelope, 'searchYear');

        let speechText = 'This has not been implemented yet, but here goes. Searching for all logs on ' 
            + searchDay + 'th ' + searchMonth + ', ' + searchYear + '.....';

        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};

// Runs when NamebookIntent is called - Search for records that satisfy the name conditions
const NamebookIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'NamebookIntent';
    },
    async handle(handlerInput) {
        const searchName = Alexa.getSlotValue(handlerInput.requestEnvelope, 'searchName');
        let speechText = 'This has not been implemented yet, but here goes. Searching for ' + searchName + '.....';

        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};


// Runs when the LaunchRequest is called - Starts the Contact Tracing skill
const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {

        // Prompt the user to input information
        const speechText = 'Welcome to the Contact Tracing skill. Please provide your name, contact information '
            +'and visitor status, in that order. Alternatively, you can search through the logbook';
        // If the user incorrectly tries to log information, give an example and reprompt the user for input
        const repromptText = 'add Alexa. That is what I would say to add my name. What is your name?'
            +' However, if I wanted to search, I would say name of or date of';    
        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .reprompt(repromptText)
            .getResponse();
    }
};


const ErrorHandler = {
    canHandle(handlerInput) {
        return true;
    },
    handle(handlerInput, error) {
        console.log('Error handled: ' + JSON.stringify(error.message));

        const speechText = 'Sorry, your skill encountered an error';
        return handlerInput.responseBuilder
            .speak(speechText)
            .withShouldEndSession(false)
            .getResponse();
    }
};
